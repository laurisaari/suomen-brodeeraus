<?php
declare(strict_types=1);

namespace App\Model;

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Log\Log;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;
use Cake\Http\Client;
use Cake\Datasource\ConnectionManager;
use DateTime;

/**
 * Integration logic for moving product stock data from Maestro API to customized Prestashop API
 */
class StockIntegration
{
    private $shopifyAPI = null;
    private $tehdenAPI = null;
    private $tehdenDefConfig = [];
    private $tehdenUrlParams = [];
    private $tehdenToken = null;
    private $tehdenUrl = null;

    /**
     * Setup integration pipeline
     *
     * @param \Psr\Http\Client\ClientInterface|\Cake\Http\Client|null $shopifyAPI HTTP client for Shopify REST API
     * @param \Psr\Http\Client\ClientInterface|\Cake\Http\Client|null $tehdenAPI HTTP client for Tehden REST API
     */
    public function __construct(
        $shopifyAPI, 
        $tehdenAPI
    ) {
        //Shopify client 
        $this->shopifyAPI = $shopifyAPI;

        //Tehden client
        $this->tehdenAPI = $tehdenAPI;

        $this->tehdenUrl = Configure::read('Tehden.api.url');

        $this->tehdenUrlParams = [
            'grant_type' => 'password',
            'username' => Configure::read('Tehden.api.username'),
            'password' => Configure::read('Tehden.api.password'),
            'redirect_url' => Configure::read('Tehden.api.redirect_url'),
            'client_id' => Configure::read('Tehden.api.client_id'),
            'client_secret' => Configure::read('Tehden.api.client_secret'),
        ];

        $this->tehdenToken = $this->tehdenAPI->get(
                'https://suomenbrodeeraus.tehden.com/oauth/oaclient/token', // URL
                    $this->tehdenUrlParams, // URL "data" = query parameters
                    $this->tehdenDefConfig,
                );
        $this->tehdenToken = $this->tehdenToken->getJson();
        $this->tehdenToken = $this->tehdenToken['access_token'];
        $this->tehdenToken = base64_encode($this->tehdenToken);

        $this->tehdenDefConfig['headers'] = [
            'Accept' => 'application/vnd.tehden.api-v1+json',
            'Accept-language' => 'fi',
            'Authorization' => 'Bearer '. $this->tehdenToken,
        ];

        //Database connection
        $this->conn = ConnectionManager::get('default');
    }

    /**
     * Main method of the stock update pipeline
     *
     * @param string $since String of update range start, will be converted to DateTime object
     *
     * @throws \Exception Throws Exception on error
     *
     * @return bool True on successful update
     */
    public function updateStocks($since = '-1 day')
    {
        if (!$this->shopifyAPI) {
            throw new \Exception('Shopify client missing', 1);
        }

        if (!$this->tehdenAPI) {
            throw new \Exception('Tehden cliet missing', 1);
        }

        if (!$this->conn) {
            throw new \Exception('Database connection missing', 1);
        }

        // Convert 'since' parameter to ISO8601 string
        $sinceParsed = null;
        if (!empty($since)) {
            // Try parsing since range start, let it crash if incorrent/unparseable
            $sinceParsed = (new \DateTime($since))->format(
                \DateTimeInterface::ISO8601
            );
            // TODO Sanity check that parsed date is not in future or too far in past
            $sinceParsed = strtotime($since);
        }

        $locationID = Configure::read('Shopify.api.location');

        $products = $this->getStockAmounts($sinceParsed);
        
        foreach ($products as $product) {
            $inventoryID = $this->getInventoryID($product);
            if ($inventoryID) {
                $amount = intval($product['amount']);
                $data = [
                    "location_id" => $locationID,
                    "inventory_item_id" => $inventoryID,
                    "available" => $amount,
                ];
                $result = $this->shopifyAPI->rest('POST', '/admin/inventory_levels/set.json', $data);
            } else {
                continue;
            }
        }
    }

    public function getStockAmounts($sinceParsed)
    {
        if (!empty($sinceParsed)) {
            $this->tehdenUrlParams['timestamp_since'] = $sinceParsed;
        }

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'stock/getStockAmounts', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $result = $result->getJson();

    return $result;
    }

    public function getInventoryID($product)
    {
        $result = $this->conn->execute('SELECT shopify_inventory_id FROM products_connector WHERE tehden_product_id = :id', ['id' => $product['product_id']])->fetchAll('assoc');
        if (!empty($result) && $result[0]['shopify_inventory_id'] != 0) {
            $inventoryID = $result[0]['shopify_inventory_id'];
        } else {
            return false;
        }
    return $inventoryID;
    }
}    