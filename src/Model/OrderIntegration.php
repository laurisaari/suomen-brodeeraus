<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\ApiClientMagento;
use App\Model\ApiClientLemonsoft;
use App\Model\ApiProductLemonsoft;
use App\Model\ApiCustomerLemonsoft;
use App\Model\ApiSalesOrderLemonsoft;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

/**
 * Integration logic for moving product stock data from Maestro API to customized Prestashop API
 */
class OrderIntegration
{
    private $shopifyAPI = null;
    private $tehdenAPI = null;
    private $tehdenDefConfig = [];
    private $tehdenUrlParams = [];
    private $tehdenToken = null;
    private $tehdenUrl = null;

    /**
     * Setup integration pipeline
     *
     * @param \Psr\Http\Client\ClientInterface|\Cake\Http\Client|null $shopifyAPI HTTP client for Shopify REST API
     * @param \Psr\Http\Client\ClientInterface|\Cake\Http\Client|null $tehdenAPI HTTP client for Tehden REST API
     */
    public function __construct(
        $shopifyAPI, 
        $tehdenAPI
    ) {
        //Shopify client 
        $this->shopifyAPI = $shopifyAPI;

        //Tehden client
        $this->tehdenAPI = $tehdenAPI;

        $this->tehdenUrl = Configure::read('Tehden.api.url');

        $this->tehdenUrlParams = [
            'grant_type' => 'password',
            'username' => Configure::read('Tehden.api.username'),
            'password' => Configure::read('Tehden.api.password'),
            'redirect_url' => Configure::read('Tehden.api.redirect_url'),
            'client_id' => Configure::read('Tehden.api.client_id'),
            'client_secret' => Configure::read('Tehden.api.client_secret'),
        ];

        $this->tehdenToken = $this->tehdenAPI->get(
                'https://suomenbrodeeraus.tehden.com/oauth/oaclient/token', // URL
                    $this->tehdenUrlParams, // URL "data" = query parameters
                    $this->tehdenDefConfig,
                );
        $this->tehdenToken = $this->tehdenToken->getJson();
        $this->tehdenToken = $this->tehdenToken['access_token'];
        $this->tehdenToken = base64_encode($this->tehdenToken);

        $this->tehdenDefConfig['headers'] = [
            'Accept' => 'application/vnd.tehden.api-v1+json',
            'Accept-language' => 'fi',
            'Authorization' => 'Bearer '. $this->tehdenToken,
        ];

        //Database connection
        $this->conn = ConnectionManager::get('default');
    }

    /**
     * Main method of the stock update pipeline
     *
     * @param string $since String of update range start, will be converted to DateTime object
     *
     * @throws \Exception Throws Exception on error
     *
     * @return bool True on successful update
     */
    public function updateOrders($since = '-1 day'): bool
    {
         if (!$this->shopifyAPI) {
            throw new \Exception('Shopify client missing', 1);
        }

        if (!$this->tehdenAPI) {
            throw new \Exception('Tehden cliet missing', 1);
        }

        if (!$this->conn) {
            throw new \Exception('Database connection missing', 1);
        }

        // Convert 'since' parameter to ISO8601 string
        $sinceParsed = null;
        if (!empty($since)) {
            // Try parsing since range start, let it crash if incorrent/unparseable
            $sinceParsed = (new \DateTime($since))->format(
                \DateTimeInterface::ISO8601
            );
            // TODO Sanity check that parsed date is not in future or too far in past
            // $sinceParsed = strtotime($since);
        }
        // dd($this->getBranches());

        $orders = $this->getOrders($sinceParsed);

        if (!$orders) {
            return true;
        }

        foreach ($orders as $order) {

            if ($order['confirmed'] === true) {

                $customerID = $this->getCustomerID($order);

                $customerData = $this->createCustomerData($order);

                if (!$customerID) {
                    $customerID = $this->createCustomer($customerData);
                } else {
                    $newCustomer = $this->updateCustomer($customerData, $customerID);
                }

                foreach ($customerData['addresses'] as $type => $address) {
                    $contactInfo = $this->createContactData($address, $type, $order['contact_email']);
                    $newContactInfo = $this->createContactInfo($customerID, $contactInfo);
                }

                $this->updateCustomerToDatabase($customerData, $customerID);

                $orderData = $this->createOrderData($order, $customerID);

                $tehdenOrder = $this->createOrder($orderData);

                $this->sendOrderToDatabase($order, $tehdenOrder);
            }
        }  
    return true;
    }

    public function createOrderData($order, $customerID)
    {
        $shippingDetail = $order['shipping_address'];
        $orderRows = $this->getOrderRows($order['line_items']);
        $orderTags = explode(',', $order['tags']);
        $deliveryID = $this->getRightDeliveryID($order);

        $saleOrder = [
            "branch_id" => "a562ac6a-e5ff-11ea-add9-fa163ec26693", // Testi branch
            "paymentterm_id" => "b21ac450-9d65-11e2-81db-4f2c9d52eb07", // 14pv netto vakiona
            "customer_id" => $customerID,
            "saledeliverymethod_id" => $deliveryID, 
            // "orderdate" => "Varmaan saa tulla atuomaattisesti",
            // "deliverydate" => "ei varmaan tarvita",
            // "backorderdate" => "ei varmaan tarvita",
            "customerorder" => $order['order_number'],
            "customerreference" => $order['id'], //"Jokin tieto asiakkaasta",
            "customernote" => "TESTITESTITESTI".$order['note'],
            "description" => "TESTITESTITESTIkuvaus asiakkaalle koskien tilausta",
            "note" => "TESTITESTITESTIkuvaus henkilökunnalle koskien tilausta " . $order['tags'],
            // "customercontractid" => "Sopimus numero",
            //"isbranchpaying" => false, //"maksaako rahdin branch vai asiakas, vakiona false",
            // "prioritized" => false, //"jos true niin menee jonon kärkeen"
            "deliveryaddress" => [
                "streetaddress" => $shippingDetail['address1'],
                "streetaddress2" => $shippingDetail['address2'],
                "streetaddress3" => $shippingDetail['address1'],
                "department" => $shippingDetail['company'],
                "postalcode" => $shippingDetail['zip'],
                "city" => $shippingDetail['city'],
                "phone" => $shippingDetail['phone'],
                "email" => $order['contact_email'],
                "name" => $shippingDetail['name'],
                "countrycode" => $shippingDetail['country_code'],
                "addresscountry_id" => "1",
            ],
            "deliverypickuppoint" => [
                // "Name" => "Pakettiautomaatti, K-citymarket Kupittaa",
                // "Id" => "Pakettiautomaatti, K-citymarket Kupittaa",
                // "RoutingServiceCode" => "3201",
                // "StreetAddress" => "Uudenmaantie 17",
                // "PostalCode" => "20705",
                // "City" => "Turku",
                // "LabelName" => "c/o Automaatti, K-citymarket Kupittaa"
            ],
            // "ledgeraccountpair_id" => "own use",
            // "purchaseccount_id" => "own use",
            // "stockexpenseaccount_id" => "own use",
            "vatstatus_id" => "1",//"1 = homeland, 2, construction reverse-tax, 3 = EU community sales good, 4 = EU community sales services, 5 EU non community sale, 6 = EU tri-party sale, 7 = Outside of EU sale",
        ];

        $amount = $order['current_subtotal_price'] * 1.24;
        $amount = round($amount, 2);

        $payment = [
            "amount" => $amount,
            "paymenttype_id" => "1",
        ];
        $payment = [$payment];

        $orderData['saleorder'] = $saleOrder;
        $orderData['rows'] = $orderRows;
        $orderData['payments'] = $payment;

    return $orderData;
    }

    public function createOrder($orderData)
    {
        //dd($orderData);
        $this->tehdenUrlParams['saleorder'] = $orderData['saleorder']; // A sale order as json object.
        $this->tehdenUrlParams['rows'] = $orderData['rows']; // An array of sale order rows as json objects. Optional.
        //$this->tehdenUrlParams['payments'] = $orderData['payments']; //An array of payments as json objects. If payments have values sale will be created and prepayment will be made. Currently only one payment type is allowed optional
        // $this->tehdenUrlParams['moneystorageline'] = $moneyStorageLine; // optional

        $result = $this->tehdenAPI->post(
            $this->tehdenUrl.'saleorder/createSaleOrderAndRows', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $order = $result->getJson();

        if ($order['code'] === 'fail') {
            return false;
        }

    return $order['context'];
    }

    public function getOrder($id)
    {
        $this->tehdenUrlParams['saleorder_id'] = $id; // A sale order as json object.

        $result = $this->tehdenAPI->get(
            $this->tehdenUrl.'saleorder/getSaleOrderInfo', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $order = $result->getJson();

        if ($order['code'] === 'fail') {
            return false;
        }

    return $order;
    }

    public function getCustomerID($order)
    {
        $businessID = '';
        if (!empty($businessID)) {
            $identifier = '123123'; // Haetaan tilauksen tiedoista Y-tunnus jos sellainen on, Y-tunnus on merkki B2B asiakkaasta.
        } else {
            $identifier = $order['contact_email']; // B2C asiakkaille haetaan tiedot tietokannasta sähköpostin avulla.
        }

        $customerID = $this->getCustomerIdByIdentifier($identifier);

    return $customerID;
    }


    public function createCustomerData($order)
    {
        $businessID = '';
        $companyID = '';

        $m = $order['buyer_accepts_marketing'];
        if ($m === false) {
            $m = "false";
        } else {
            $m = "true";
        }
        $marketing = strval($m);
        $marketing = substr($marketing, 0, 1);
        $customer = $order['customer'];
        $default = $customer['default_address'];
        $billing = $order['billing_address'];
        $shipping = $order['shipping_address'];
        $customerID = $customer['id'];

        if (!empty($businessID)) {
            $customerType = 'customertype_company';
            $firstname = '';
            $lastname = $default['company'];
            $identifier = $businessID;
        } else {
            $customerType = 'customertype_person';
            $firstname = $default['first_name'];
            $lastname = $default['last_name'];
            $identifier = $order['contact_email'];
        }

        // Tehdään asiakas ja sen jälkeen contacti inffot liitetään asiakkaaseen
        $customerData = [
            "customer" => [
                "firstname" => $firstname, // Ei tarvi jos firma
                "lastname" => $lastname, // Tai firman nimi jos firma
                "gender" => "m", // mies oletettu aina
                //"birthdate" => "", // Ei tarvita
                "mailnews" => $marketing,
                "emailnews" => $marketing,
                "smsnews" => $marketing,
                "mailads" => $marketing,
                "emailads" => $marketing,
                "smsads" => $marketing,
                // "permissionasked" => "f",
                "businessid" => $businessID,
                // "organisationunitcode" => "", // ei hajuakaa
                "identifier" => $identifier,
                // "vatid" => "", // Vero juttuja?
                // "homebranch_id " => "", //oti branch
                // "parentcustomer_id" => "", // Voidaan kai käyttää siihen että liitetään asiakkaita yhtiöihin?
                // "externalsystem_id" => "", // Luultavasti 7
                // "externalsystemidentifier" => "", // Verkkokuppa indetifioija
                // "billedcustomer_id" => "", // Asiakas ID johon laskut menisi automaatiolla
                // "company_id" => "", // Tähän voi laittaa Tehdenin company Id jos halutaan liittää
            ],
            "customer_type" => $customerType,
            "addresses" => [
                "default" => $default,
                "billing" => $billing,
                "shipping" => $shipping,
            ],
            "email" => $order['contact_email'],
        ];

        //dd($customerData);
    return $customerData;
    }


    public function getRightDeliveryID($order)
    {   
        $mapping = Configure::read('deliveryMethods');

        $method = "1";

        $deliveryID = array_search($method, $mapping);

    return $deliveryID;
    }

    public function createContactData($address, $type, $email)
    {
        if ($type == "default") {
            $type = "postal_address";
        } else if ($type == "billing") {
            $type = "billing_address";
        } else if ($type == "shipping") {
            $type = "delivery_address";
        }

        $contactInfo = [
            'email' => $email, 
            'phone' => $address['phone'], 
            'streetaddress' => $address['address1'],
            'streetaddress2' => $address['address2'],
            'streetaddress3' => $address['phone'],
            'postalcode' => $address['zip'],
            'city' => $address['city'],
            'country' => $address['country'],
            'addresstype' => $type
        ];
    return $contactInfo;
    }

    public function createContactInfo($customerID, $contactInfo)
    {
        $this->tehdenUrlParams['customer_id'] = $customerID; 
        $this->tehdenUrlParams['contact_info'] = $contactInfo;

        $result = $this->tehdenAPI->post(
            $this->tehdenUrl.'company_customer/createTehdenCustomerContactInfo', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );
        $newContact = $result->getJson();

    return $newContact;
    }

    public function setContactInfo($customerID, $contactInfo)
    {
        $this->tehdenUrlParams['customer_id'] = $customerID; 
        $this->tehdenUrlParams['address'] = $contactInfo;

        $result = $this->tehdenAPI->post(
            $this->tehdenUrl.'company_customer/setTehdenCustomerAddress', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );
        $newCustomer = $result->getStatusCode();
        
        if ($newCustomer == 200) {
            return true;
        } else {
            return false;
        }
    }

    public function updateCustomerToDatabase($customerData, $customerID)
    {
        if ($customerData['customer_type'] == 'customertype_person') {
            $type = 'person';
        } else if ($customerData['customer_type'] == 'customertype_company') {
            $type = 'company';
        } else {
            $type = $customerData['customer_type'];
        }

        $result = $this->conn->execute('SELECT tehden_id FROM customers WHERE tehden_id = :id', ['id' => $customerID])->fetchAll('assoc');
        if (empty($result)) {
            $this->conn->insert('customers', [
                'tehden_id' => $customerID,
                'shopify_id' => $customerData['addresses']['default']['customer_id'],
                'name' => $customerData['customer']['firstname'],
                'lastname' => $customerData['customer']['lastname'],
                'type' => $type,
                'businessid' => $customerData['customer']['businessid'],
                'email' => $customerData['email'],
                'phone' => $customerData['customer']['phone'],
            ]);
        } else {
            $this->conn->update('customers', [
                'shopify_id' => $customerData['addresses']['default']['customer_id'],
                'name' => $customerData['customer']['firstname'],
                'lastname' => $customerData['customer']['lastname'],
                'type' => $type,
                'businessid' => $customerData['customer']['businessid'],
                'email' => $customerData['email'],
                'phone' => $customerData['addresses']['default']['phone'],
            ], ['tehden_id' => $customerID]);       
        }
    return true;
    }

    public function getOrders($sinceParsed)
    {
        $params = ['updated_at_min' => $sinceParsed];
        $result = $this->shopifyAPI->rest('GET', '/admin/orders.json', $params);

        if ($result['body'] === "Not Found") {
            return false;
        }
        $orders = $result['body']->container['orders'];

    return $orders;
    }


    public function getCustomers($ids)
    {   
        if (!empty($ids)) {
            $this->tehdenUrlParams['customer_ids'] = $ids;
        }

        $result = $this->tehdenAPI->get(
            $this->tehdenUrl.'company_customer/getCustomers', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $customers = $result->getJson();

        if (empty($customers)) {
            return false;
        }
        $customers = $customers['result'];

    return $customers;
    }

    public function getBranches()
    {

        $result = $this->tehdenAPI->get(
            $this->tehdenUrl.'branch/getAllBranches', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $branches = $result->getJson();

        if (empty($branches)) {
            return false;
        }

    return $branches;
    }

    public function getCustomerIdByIdentifier($identifier)
    {
        $this->tehdenUrlParams['identifier'] = $identifier;
        $result = $this->tehdenAPI->get(
            $this->tehdenUrl.'company_customer/getCustomerIdByIdentifier', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $customer = $result->getJson();

    return $customer;
    }

    public function getPaymentTerms()
    {
        $result = $this->tehdenAPI->get(
            $this->tehdenUrl.'invoicing/getPaymentTerms', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $payments = $result->getJson();

    return $payments;
    }

    public function getAllDeliveryMethods()
    {
        $result = $this->tehdenAPI->get(
            $this->tehdenUrl.'saledeliverymethod/getAll', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $deliveryMethods = $result->getJson();

        $deliveryMethods = $deliveryMethods['saledeliverymethods'];

    return $deliveryMethods;
    }

    public function getDeliveryMethod($id)
    {
        $this->tehdenUrlParams['saledeliverymethod_id'] = $id;
        $result = $this->tehdenAPI->get(
            $this->tehdenUrl.'saledeliverymethod/get', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $deliveryMethod = $result->getJson();

    return $deliveryMethod;
    }

    public function createCustomer($customerData)
    {
        $this->tehdenUrlParams['customer'] = $customerData['customer'];
        $this->tehdenUrlParams['customertype_key'] = $customerData['customer_type']; //customertype_person for people. customertype_company for companies.
        // $this->tehdenUrlParams['company_id'] = $companyID; // optional

        $result = $this->tehdenAPI->post(
            $this->tehdenUrl.'company_customer/createTehdenCustomer', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $newCustomer = $result->getJson();

    return $newCustomer['customer_id'];
    }

    public function updateCustomer($customerData, $customerID)
    {
        $this->tehdenUrlParams['customer_id'] = $customerID;
        $this->tehdenUrlParams['data'] = $customerData['customer'];

        $result = $this->tehdenAPI->post(
            $this->tehdenUrl.'company_customer/updateTehdenCustomer', // URL
            $this->tehdenUrlParams, // URL "data" = query parameters
            $this->tehdenDefConfig,
        );

        $updatedCustomer = $result->getJson();

        if ($updatedCustomer['code'] === "success") {
            return true;
        } else {
            return false;
        }
    }

    public function getOrderRows($items)
    {
        $rows = [];
        $row = [];
        foreach ($items as $item) {
            if ($item['fulfillment_status'] == "fulfilled") {
                $tehdenID = $this->getProductId($item['variant_id']);
                $row = [
                    "product_id" => $tehdenID,
                    "amount" => $item['quantity'],
                ];
                $rows[] = $row;
            }
        }
    return $rows;
    }

    public function sendOrderToDatabase($shopifyOrder, $tehdenOrder)
    {
        $status = 'confirmed';

        $result = $this->conn->execute('SELECT * FROM orders_status WHERE order_id_shopify = :id', ['id' => $shopifyOrder['id']])->fetchAll('assoc');

        if (empty($result)) {
            $this->conn->insert('orders_status', [
                'order_id_shopify' => $shopifyOrder['id'],
                'order_id_tehden' => $tehdenOrder['saleorder_id'],
                'status' => $status,
                'order_number_shopify' => $shopifyOrder['order_number'],
                'order_number_tehden' => $tehdenOrder['ordernumber'],
                'saleorder_state_shopify' => $shopifyOrder['fulfillment_status'],
                'saleorder_state_tehden' => $tehdenOrder['saleorderstate_id'],
            ]);
        } else {
            $this->conn->update('orders_status', [
                'status' => $status,
                'saleorder_state_shopify' => $shopifyOrder['fulfillment_status'],
                'saleorder_state_tehden' => $tehdenOrder['saleorderstate_id'],
            ], ['order_id_shopify' => $shopifyOrder['id']]);       
        }

        $rows = $tehdenOrder['rows'];
        foreach ($shopifyOrder['line_items'] as $row) {
            $result = $this->conn->execute('SELECT tehden_product_id FROM products_connector WHERE shopify_product_id = :id', ['id' => $row['variant_id']])->fetchAll('assoc');
            $tehdenID = $result[0]['tehden_product_id'];
            $tehdenRow = $this->getRightOrderRow($rows, $tehdenID);
            $result = $this->conn->execute('SELECT * FROM order_rows WHERE row_id_shopify = :id', ['id' => $row['id']])->fetchAll('assoc');

            if (empty($result)) {
                $this->conn->insert('order_rows', [
                    'saleorder_id_shopify' => $shopifyOrder['id'],
                    'saleorder_id_tehden' => $tehdenRow['saleorder_id'],
                    'row_id_shopify' => $row['id'],
                    'row_id_tehden' => $tehdenRow['saleorderrow_id'],
                    'product_name_shopify' => $row['name'],
                    'product_name_tehden' => $tehdenRow['name'],
                    'product_id_shopify' => $row['variant_id'],
                    'product_id_tehden' => $tehdenRow['product_id'],
                    'amount_shopify' => $row['price'],
                    'amount_tehden' => $tehdenRow['total'],
                ]);
            }
        }
    return true;
    }

    public function getRightOrderRow($rows, $tehdenID)
    {
        foreach ($rows as $row) {
            if ($row['product_id'] === $tehdenID) {
                return $row;
            }
        }
    return false;
    }

    public function getProductId($shopifyID)
    {
        $result = $this->conn->execute('SELECT tehden_product_id FROM products_connector WHERE shopify_product_id = :id', ['id' => $shopifyID])->fetchAll('assoc');
        if (!empty($result) && $result[0]['tehden_product_id'] != 0) {
            $tehdenID = $result[0]['tehden_product_id'];
        } else {
            return false;
        }
    return $tehdenID;
    }
}