<?php
declare(strict_types=1);

namespace App\Model;

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Log\Log;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;
use Cake\Http\Client;
use Cake\Datasource\ConnectionManager;
use DateTime;

/**
 * Integration logic for moving product stock data from Maestro API to customized Prestashop API
 */
class ProductIntegration
{
    private $shopifyAPI = null;
    private $tehdenAPI = null;
    private $tehdenDefConfig = [];
    private $tehdenUrlParams = [];
    private $tehdenToken = null;
    private $tehdenUrl = null;
    private $productGroups = null;


    /**
     * Setup integration pipeline
     *
     * @param \Psr\Http\Client\ClientInterface|\Cake\Http\Client|null $shopifyAPI HTTP client for Shopify REST API
     * @param \Psr\Http\Client\ClientInterface|\Cake\Http\Client|null $tehdenAPI HTTP client for Tehden REST API
     */
    public function __construct(
        $shopifyAPI, 
        $tehdenAPI
    ) {
        //Shopify client 
        $this->shopifyAPI = $shopifyAPI;

        //Tehden client
        $this->tehdenAPI = $tehdenAPI;

        $this->tehdenUrl = Configure::read('Tehden.api.url');

        $this->tehdenUrlParams = [
            'grant_type' => 'password',
            'username' => Configure::read('Tehden.api.username'),
            'password' => Configure::read('Tehden.api.password'),
            'redirect_url' => Configure::read('Tehden.api.redirect_url'),
            'client_id' => Configure::read('Tehden.api.client_id'),
            'client_secret' => Configure::read('Tehden.api.client_secret'),
        ];

        $this->tehdenToken = $this->tehdenAPI->get(
                'https://suomenbrodeeraus.tehden.com/oauth/oaclient/token', // URL
                    $this->tehdenUrlParams, // URL "data" = query parameters
                    $this->tehdenDefConfig,
                );
        $this->tehdenToken = $this->tehdenToken->getJson();
        $this->tehdenToken = $this->tehdenToken['access_token'];
        $this->tehdenToken = base64_encode($this->tehdenToken);

        $this->tehdenDefConfig['headers'] = [
            'Accept' => 'application/vnd.tehden.api-v1+json',
            'Accept-language' => 'fi',
            'Authorization' => 'Bearer '. $this->tehdenToken,
        ];

        //Database connection
        $this->conn = ConnectionManager::get('default');
    }

    /**
     * Main method of the stock update pipeline
     *
     * @param string $since String of update range start, will be converted to DateTime object
     *
     * @throws \Exception Throws Exception on error
     *
     * @return bool True on successful update
     */
    public function updateProducts($since = '-1 day')
    {
        if (!$this->shopifyAPI) {
            throw new \Exception('Shopify client missing', 1);
        }

        if (!$this->tehdenAPI) {
            throw new \Exception('Tehden cliet missing', 1);
        }

        if (!$this->conn) {
            throw new \Exception('Database connection missing', 1);
        }
        $time_start = microtime(true);

        // Convert 'since' parameter to ISO8601 string
        $sinceParsed = null;
        // if (!empty($since)) {
        //     // Try parsing since range start, let it crash if incorrent/unparseable
        //     $sinceParsed = (new \DateTime($since))->format(
        //         \DateTimeInterface::ISO8601
        //     );
        //     // TODO Sanity check that parsed date is not in future or too far in past
        //     $sinceParsed = strtotime($since);
        // }

        $ids = [];

        //dd($this->getProduct("0a68057e-67ba-11eb-bed3-fa163ec26693"));
        // $test = $this->shopifyAPI->rest('GET', '/admin/variants/.json');
        // $test = $test['body']->container['variant'];
        // $test = $this->shopifyAPI->rest('GET', '/admin/products/6586627883216.json');
        // $test = $test['body']->container['product'];
        // $test = $this->getProductGroups();
        // dd($test);
        // $shopifyProduct = $this->shopifyAPI->rest('GET', '/admin/variants/123321312.json');
        // dd($shopifyProduct);

        // Getting ids of all updated products
        // $ids = $this->getUpdatedProducts($sinceParsed);

        // Testaus iideet
        $idsasd = [
            // "95fdb28e-9161-11eb-8af9-fa163ec26693",
            // "95ff010c-9161-11eb-8af9-fa163ec26693",
            // "e4b282fc-4391-11ea-9caa-fa163e7cfb01",
            // "e4c4419a-4391-11ea-9caa-fa163e7cfb01",
            // "e4d359dc-4391-11ea-9caa-fa163e7cfb01",
            // "e4e0296e-4391-11ea-9caa-fa163e7cfb01",
            // "e4ecd77c-4391-11ea-9caa-fa163e7cfb01",
            // "e4f93350-4391-11ea-9caa-fa163e7cfb01",
            // "e506547c-4391-11ea-9014-fa163e7cfb01",
            // "e51300b4-4391-11ea-927c-fa163e7cfb01",
            //"e5206cfe-4391-11ea-9014-fa163e7cfb01",
            // "e43da220-4391-11ea-927c-fa163e7cfb01",
            // "e44b2990-4391-11ea-927c-fa163e7cfb01",
            // "e45878f2-4391-11ea-8cbf-fa163e7cfb01",
            // "e465937a-4391-11ea-9014-fa163e7cfb01",
            // "e4724b56-4391-11ea-9014-fa163e7cfb01",
            // "e47e58ce-4391-11ea-9014-fa163e7cfb01",
            // "e48ae9cc-4391-11ea-9014-fa163e7cfb01",
            // "e497cef8-4391-11ea-9014-fa163e7cfb01",
            // "e4a534f8-4391-11ea-8cbf-fa163e7cfb01",
            // "95fdb28e-9161-11eb-8af9-fa163ec26693",
        ];

        // Ending update now if there is not any ids
        // if (empty($ids)) {
        //     return true;
        // }

        // Getting data of all products from the ids
        // $products = $this->getProducts($ids);
        // // If there is too many ids we have to get data separately
        // if ($products === false) {
        //     Log::write('debug', 'Update number: '. $lastUpdate .' started');
        //     $products = [];
        //     $count = count($ids);
        //     foreach ($ids as $i => $id) {
        //         if ($i > $bottomAmount && fmod($i, 2000) === 0.0) {
        //             $this->getNewToken();
        //         }
        //         //debug($i . '/' . $count);
        //         if ($i < $bottomAmount) {
        //             continue;
        //         }
        //         $a = [$id];
        //         $product = $this->getProducts($a);

        //         if ($product === false) {
        //             continue;
        //         }
        //         $products[] = $product[0];
        //         //Breaking loop for testing purposes
        //         if ($i == $topAmount) {
        //             break;
        //         }
        //     }
        // }
        $stop = strtotime('+60 minute');
        // $now = strtotime('now');
        $ids = $this->conn->execute('SELECT id FROM products WHERE moved = 0')->fetchAll('assoc');
        
        // $ids = $this->conn->execute('SELECT tehden_product_id FROM products_connector WHERE id > 70200')->fetchAll('assoc');
        $count = count($ids);
        // print('Starting product updates for ' . $count . ' products' . "\n");

        // Possibility to optimize further!!
        // Create chunks of ids of say 50 and get their products in same request!
        $idChunks = array_chunk($ids, 50);
        $chunkCount = count($idChunks);
        debug('Created ' . $chunkCount . ' product chunks updates for ' . $count . ' products');
        $i = 1;
        $p = 1;
        foreach($idChunks as $chunkKey => $idChunk) {
            print("----------------- Starting chunk " . $chunkKey . ' / ' . $chunkCount . "---------------------\n");
            $chunk_time_start = microtime(true);
            $productIdsToFetch = [];
            foreach($idChunk as $key => $singleProductId) {
                // $currentKey = ($chunkKey + 1) * $key;
                $id = $singleProductId['id'];
                $shopifyID = $this->getShopifyID($id);

                if ($shopifyID !== false) {
                    $this->conn->update('products', [
                        'moved' => 1,
                    ], ['id' => $id]);
                    // Log::write('debug', $p . '/' . $count);
                    // Log::write('debug', 'Skipped existing product ' . $id['id'] . ' at ' . $index . '/' . $count);
                    print('Skipped existing product ' . $id . ' at ' . $key . '/' . $count . "\n");
                    // $p++;
                    // $now = strtotime('now');
                    continue;
                }
                $productIdsToFetch[] = $id;
            }

            // dd($ids);

            if ($p !== 0 && fmod($p, 300) === 0.0) {
                $getTokenStart = microtime(true);
                $this->getNewToken();
                $getTokenEnd = microtime(true);
                $getTokenTime = $getTokenEnd - $getTokenStart;
                Log::write('debug', 'Refetching Tehden token took: ' . $getTokenTime);
                print('Refetching Tehden token took: ' . $getTokenTime . "\n");
            }
            $getProductStart = microtime(true);

            if (empty($productIdsToFetch)) {
                Log::write('debug', 'No products to fetch');
                debug('No products to fetch');
                continue;
            }
            // debug($productIdsToFetch);
            $products = $this->getProducts($productIdsToFetch);
            if (!$products) {
                Log::write('debug', 'Failed fetching products from tehden');
                debug('Failed fetching products from tehden');
                continue;
            }
            $getProductEnd = microtime(true);
            $getProductTime = $getProductEnd - $getProductStart;
            Log::write('debug', 'Fetch Tehden products took: ' . $getProductTime);
            print('Fetch Tehden products took: ' . $getProductTime . "\n");

            // dd($products);
            // Going through products one by one
            foreach ($products as $index => $product) {
                print("\n" . $p . '/' . $count . "-------------------------------------->  " . $product['product_id'] . "\n");
                if ($index == 0) {
                    continue;
                }
                $prod_time_start = microtime(true);
                // if ($index < 10000) {
                //     continue;
                // }

                // $product = $product[0];
                $p++;
                //Fixejä
                //// Lisätty iffit, heitti erroria välissä. 
                $tehdenID = isset($product['product_id']) ? $product['product_id'] : null;
                $type = isset($product['producttype_key']) ? $product['producttype_key'] : null;
                /////////////////////////////
                

                // Searching for Shopify ID from database, if getting false the product is new and needs to be founded
                // $shopifyID = $this->getShopifyID($tehdenID);

                //Fixejä
                ///lisätty isset($product['availableonline']), ei välttämättä esiinny aina datassa ja heittää errorin
                if (isset($product['availableonline']) && $product['availableonline'] === "f" || $product['availableonline'] === "t") {
                    if ($shopifyID === false) {
                        // print('Shopify id is null Product took: ' . "\n");
                        // $savepPoductStart = microtime(true);
                        $result = $this->sendToDatabase($product, null);
                        // $savepPoductEnd = microtime(true);
                        // $savepPoductTime = $savepPoductEnd - $savepPoductStart;
                        // Log::write('debug', 'Save Product took: ' . $savepPoductTime);
                        // print('Save Product took: ' . $savepPoductTime . "\n");

                        if ($type == "producttype_stock_goods") {
                            if (empty($product['in_selectiongroups'])) {
                                // print('Shopify id is null createProduct: ' . "\n");
                                // $createProductStart = microtime(true);
                                // $result = $this->sendToDatabase($product, null);
                                $created = $this->createProduct($product);
                                // $createProductEnd = microtime(true);
                                // $createProductTime = $createProductEnd - $createProductStart;
                                // Log::write('debug', 'Create Product took: ' . $createProductTime);
                                // print('Create Product took: ' . $createProductTime . "\n");
                            } else {
                                // print('Shopify id is null createProductVariant: ' . "\n");
                                // $result = $this->sendToDatabase($product, null);
                                // $createProductVariantStart = microtime(true);
                                $created = $this->createProductVariant($product);
                                // $createProductVariantEnd = microtime(true);
                                // $createProductVariantTime = $createProductVariantEnd - $createProductVariantStart;
                                // Log::write('debug', 'Create Product Variant took: ' . $createProductVariantTime);
                                // print('Create Product Variant took: ' . $createProductVariantTime . "\n");
                            }
                        } elseif ($type == "producttype_selection") {
                            // print('Shopify id is null createProductWithVariants: ' . "\n");
                            // $result = $this->sendToDatabase($product, null);
                            // $createProductWithVariantStart = microtime(true);
                            $created = $this->createProductWithVariants($product);
                            // $createProductWithVariantEnd = microtime(true);
                            // $createProductWithVariantTime = $createProductWithVariantEnd - $createProductWithVariantStart;
                            // Log::write('debug', 'Create Product with Variant took: ' . $createProductWithVariantTime);
                            // print('Create Product with Variant took: ' . $createProductWithVariantTime . "\n");
                        } else {
                            // Jokin muu tuote tyyppi
                            // $result = $this->sendToDatabase($product, null);
                            // Log::write('debug', 'New product type ' . $tehdenID . ' ' . $type);
                            print('New product type ' . $tehdenID . ' ' . $type . "\n");
                        }
                    } else {
                        // print('Shopify id is NOT null and not doing anything?: ');
                        debug('New product type ' . $tehdenID . ' ' . $type . " not sending to shopify!! \n");
                        continue;
                        // if ($type == "producttype_stock_goods") {
                        //     if (empty($product['in_selectiongroups'])) {
                        //         continue;
                        //         // $result = $this->updateProduct($product, $shopifyID);
                        //     } else {
                        //         $result = $this->updateVariant($product, $shopifyID);
                        //     }
                        // } elseif ($type == "producttype_selection") {
                        //     continue;
                        //     // $result = $this->updateProductWithVariants($product, $shopifyID);
                        // } else {
                        //     // Jokin muu tuote tyyppi
                        //     $result = $this->sendToDatabase($product, null);
                            // Log::write('debug', 'New product type ' . $tehdenID . ' ' . $type);
                        // }
                    }
                    if (!$result) {
                        // Log::write('debug', 'Something did not work ' . $tehdenID . ' ' . $type);
                        print('Something did not work ' . $tehdenID . ' ' . $type . "\n");
                    }
                    // $now = strtotime('now');
                    $i++;
                    //Fixejä
                    ////lisätty isset($product['availableonline']), ei välttämättä esiinny aina datassa ja heittää errorin
                } elseif (isset($product['availableonline']) && $product['availableonline'] === "tjiksdhaskjldh") {
                    print('Available online is tjiksdhaskjldh and deleting product? '. "\n");
                    if ($shopifyID) {
                        if ($type == "producttype_stock_goods") {
                            if (empty($product['in_selectiongroups'])) {
                                $result = $this->deleteProduct($product, $shopifyID);
                            } else {
                                $result = $this->deleteProductVariant($product, $shopifyID);
                            }
                        } elseif ($type == "producttype_selection") {
                            $result = $this->deleteProductWithVariants($product, $shopifyID);
                        } else {
                            // Jokin muu tuote tyyppi
                            // Log::write('debug', 'New product type ' . $tehdenID . ' ' . $type);
                            print('New product type ' . $tehdenID . ' ' . $type . "\n");
                        }
                    } else {
                        $prod_time_end = microtime(true);
                        $prod_time = $prod_time_end - $prod_time_start;
                        print($prod_time . "\n");
                        continue;

                    }
                    if (!$result) {
                        // Log::write('debug', 'Something did not work when deleting product ' . $tehdenID . ' ' . $type);
                        print('Something did not work when deleting product ' . $tehdenID . ' ' . $type . "\n");
                    }
                }
                $prod_time_end = microtime(true);
                $prod_time = $prod_time_end - $prod_time_start;
                print('Product total time:' . $prod_time . "\n");
            }

            $chunk_time_end = microtime(true);
            $chunk_time = $chunk_time_end - $chunk_time_start;

            $time_end = microtime(true);
            $time = $time_end - $time_start;
            print('Chunk total time:' . $chunk_time . ' and Current total at: ' . $time . "\n");
            Log::write('debug', $p . '/' . $count);
            // debug($p . '/' . $count . "\n");
        }

        // $time_end = microtime(true);
        // $time = $time_end - $time_start;
        print('Script done in: ' . $time . 'seconds' . "\n");
        Log::write('info', 'Script done in: ' . $time . 'seconds');
        return true;
    }

    public function createProduct($product)
    {
        $description = $this->getRightDescription($product['descriptions']);
        $state = $this->getState($product['productstate']);
        $color = $this->getColor($product);
        $type = $this->getType($product['productgroups']);
        $brand = $this->getBrand($product['productgroups']);

        $image = $this->getImage($product['images']);

        $tags = '';
        $tags .= $color;

        $mainData = [
            "product" => [
                "title" => $product['name'],
                "body_html" => $description,
                "status" => "active", // Tähän joku funktio
                "product_type" => $type,
                "vendor" => $brand,
                "tags" => $tags,
                "variants" => [
                    [
                        "title" => $product['name'],
                        "price" => $product['price'],
                        "sku" => $product['code'],
                        "weight" => $product['unitweight'],
                        "weight_unit" => "kg",
                        "inventory_management" => "shopify",
                        "inventory_policy" => "continue",
                        "taxable" => true,
                    ],
                ],
            ],
        ];

        $result = $this->shopifyAPI->rest('POST', '/admin/products.json', $mainData);
        // print_r($result);
        if ($result['errors'] === true) {
            Log::write('debug', 'Failed to create product: ' . $product['product_id']);
            $logData = print_r($result["body"],true);
            Log::write('debug', $logData);
            // $logData = print_r($mainData,true);
            // Log::write('debug', $logData);
            return false;
        }
        $newProduct = $result['body']->container['product'];


        $done = $this->sendToDatabase($product, $newProduct);
        $imageID = $this->addImage($image, $newProduct);
    return true;
    }

    public function createProductWithVariants ($product)
    {
        // $state = $this->getState($product['productstate']);
        $state = $this->getState($product['productstate']);
        $brand = $this->getBrand($product['productgroups']);
        $type = $this->getType($product['productgroups']);
        $color = $this->getColor($product);
        $similarity = $this->getSimilarity($product);
        $optionType = $this->getOptionType($product);
        $image = null;
        $description = null;

        $tags = '';
        $tags .= $color;

        $mainData = [
            "product" => [
                "title" => $product['name'],
                "body_html" => $description,
                "status" => "active", 
                "product_type" => $type,
                "vendor" => $brand,
                "tags" => $tags,
                "metafields" => [
                    [
                        "key" =>"product_group",
                        "value" => $similarity,
                        "value_type" => "string",
                        "namespace" => "global"
                    ]
                ],
                "variants" => [
                ],
                "options" => [
                    "name" => $optionType
                ],
            ],
        ];

        $selectionGroup = $product['selectionproducts'];
        $skip = Configure::read('Skip');

        $variantProductsToFetch = [];
        foreach ($selectionGroup as $key => $g) {
            // $data = [];
            if (in_array($g, $skip)) {
                continue;
            }

            $variantProductsToFetch[] = $g;
        }
        // Create chunks to only fetch 50 variants at a time
        $variantChunks = array_chunk($variantProductsToFetch, 50);
        $variantChunksCount = count($variantChunks);
        print('Created ' . $variantChunksCount . ' variant chunks for ' . count($variantProductsToFetch) . ' variants' . "\n");
        // dd($variantProductsToFetch);

        $parentProduct = null;
        foreach($variantChunks as $variantChunkKey => $variantChunk) {
            // dd($variantChunk);
            $getProductStart = microtime(true);
            $tehdenVariants = $this->getProducts($variantChunk);
            $getProductEnd = microtime(true);
            $getProductTime = $getProductEnd - $getProductStart;
            print('Get tehden variants took: ' . $getProductTime . "\n");
            // dd($tehdenVariants);
            foreach ($tehdenVariants as $key => $tehdenVariant) {
                // $selectionGroupStart = microtime(true);
                $data = [];

                if (!empty($tehdenVariant['in_selectiongroups'][0]['id'])) {
                    $parentId = $tehdenVariant['in_selectiongroups'][0]['id'];
                    if (!isset($parentProduct) || $parentId != $parentProduct['product_id']) {
                        print('Fetching tehden parent product -> ' . $parentId . "\n");
                        // debug($tehdenVariant['in_selectiongroups'][0]['id']);
                        // $getParentProductStart = microtime(true);
                        // $product = $this->getProducts($a);
                        $parentProduct = $this->getProduct($parentId);
                        // dd($parentProduct);
                        // $getParentProductEnd = microtime(true);
                        // $getParentProductTime = $getParentProductEnd - $getParentProductStart;
                        // Log::write('debug', 'Fetch Tehden product took: ' . $getParentProductTime);
                        // print('Fetch Tehden parent product for all -> ' . $parentId . ' <-took: ' . $getParentProductTime . "\n");
                    }
                }

                // $selectionGroupInnerStart = microtime(true);
                $option = $this->getOptionValue($product['code'], $tehdenVariant['code']);
                $state = $this->getState($tehdenVariant['productstate']);
                if ($image === null) {
                    $image = $this->getImage($tehdenVariant['images']);
                }
                if ($mainData["product"]["body_html"] === null) {
                    $description = $this->getRightDescription($tehdenVariant['descriptions']);
                    $mainData["product"]["body_html"] = $description;
                }

                $data = [
                    "price" => $tehdenVariant['price'],
                    "sku" => $tehdenVariant['code'],
                    "option1" => $option,
                    "weight" => $tehdenVariant['unitweight'],
                    "weight_unit" => "kg",
                    "inventory_management" => "shopify",
                    "inventory_policy" => "continue",
                    "taxable" => true,
                    "metafields" => [
                        [
                            "key" =>"product_group",
                            "value" => $similarity,
                            "value_type" => "string",
                            "namespace" => "global"
                        ]
                    ],
                ];
                // $selectionGroupInnerEnd = microtime(true);
                // $selectionGroupInnerTime = $selectionGroupInnerEnd - $selectionGroupInnerStart;
                // print('selectionGroupInner took: ' . $selectionGroupInnerTime . "\n");

                // $sendToDatabaseStart = microtime(true);
                $done = $this->sendToDatabase($tehdenVariant, null, $parentProduct);
                // $sendToDatabaseEnd = microtime(true);
                // $sendToDatabaseTime = $sendToDatabaseEnd - $sendToDatabaseStart;
                // print('Save to database took: ' . $sendToDatabaseTime . "\n");
                
                array_push($mainData["product"]["variants"], $data);

                // $selectionGroupEnd = microtime(true);
                // $selectionGroupTime = $selectionGroupEnd - $selectionGroupStart;
                // print('selectionGroup took: ' . $selectionGroupTime . "\n");
            }
        }

        if (count($mainData["product"]["variants"]) > 100) {
            Log::write('info', 'Failed to create product, too many variants('. count($mainData["product"]["variants"]) .'). Product: ' . $product['product_id']);
            return false;
        } else {
            $shopifyPostStart = microtime(true);
            $result = $this->shopifyAPI->rest('POST', '/admin/products.json', $mainData);
            $shopifyPostEnd = microtime(true);
            $shopifyPostTime = $shopifyPostEnd - $shopifyPostStart;
            print('Send to shopifytook: ' . $shopifyPostTime . "\n");
            if ($result['errors'] === true) {
                Log::write('debug', 'Failed to create product: ' . $product['product_id']);
                $logData = print_r($result["body"],true);
                Log::write('debug', $logData);
                $logData = print_r($mainData,true);
                Log::write('debug', $logData);
                return false;
            }
            $newProduct = $result['body']->container['product'];

            // $sendToDatabaseStart = microtime(true);
            $done = $this->sendToDatabase($product, $newProduct);
            // $sendToDatabaseEnd = microtime(true);
            // $sendToDatabaseTime = $sendToDatabaseEnd - $sendToDatabaseStart;
            // print('Save to database took: ' . $sendToDatabaseTime . "\n");

            $newVariants = $newProduct["variants"];
            $i = 0;
            foreach ($newVariants as $variant) {
                // $sendVariantDataStart = microtime(true);
                $this->sendVariantData($variant, $selectionGroup[$i]);
                // $sendVariantDataEnd = microtime(true);
                // $sendVariantDataTime = $sendVariantDataEnd - $sendVariantDataStart;
                // print('Send variant took: ' . $sendVariantDataTime . "\n");

                $i++;
            }
            $addImageStart = microtime(true);
            $imageID = $this->addImage($image, $newProduct);
            $addImageEnd = microtime(true);
            $addImageTime = $addImageEnd - $addImageStart;
            print('Add image took: ' . $addImageTime . "\n");
            return true;
        }
    }

    public function createProductVariant($product)
    {
        $parentId = $product['in_selectiongroups'][0]['id'];

        $parentProduct = $this->getProduct($parentId);
        $option = $this->getOptionValue($parentProduct['code'], $product['code']);
        $description = $this->getRightDescription($product['descriptions']);
        $state = $this->getState($product['productstate']);
        $color = $this->getColor($product);
        $similarity = $this->getSimilarity($product);
        $shopifyID = null;

        $result = $this->conn->execute('SELECT shopify_product_id AND shopify_image_id FROM products_connector WHERE tehden_product_id = :id', ['id' => $parentId])->fetchAll('assoc');
        if (!empty($result)) {
            if (isset($result[0]['shopify_product_id'])) {
                $shopifyID = $result[0]['shopify_product_id'];
            }
            if (isset($result[0]['shopify_image_id'])) {
                $imageID = $result[0]['shopify_product_id'];
            }
        } 
        if ($shopifyID === null) {
            $this->createProductWithVariants($parentProduct);
            return true;
        } 
        

        $mainData = [
            "price" => $product['price'],
            "sku" => $product['code'],
            "option1" => $option,
            "weight" => $product['unitweight'],
            "weight_unit" => "kg",
            "inventory_management" => "shopify",
            "inventory_policy" => "continue",
            "taxable" => true,
            "metafields" => [
                [
                    "key" =>"product_group",
                    "value" => $similarity,
                    "value_type" => "string",
                    "namespace" => "global"
                ]
            ],
        ];

        $result = $this->shopifyAPI->rest('POST', '/admin/products/'.$shopifyID.'/variants.json', $mainData);
        if ($result['errors'] === true) {
            Log::write('debug', 'Failed to create product: ' . $product['product_id']);
            $logData = print_r($result["body"],true);
            Log::write('debug', $logData);
            $logData = print_r($mainData,true);
            Log::write('debug', $logData);
            return false;
        }
        $newProduct = $result['body']->container['variant'];

        $data = [
            "image" => [
                "id" => $imageID,
                "variant_ids" => [
                    $newProduct['id'],
                ],
            ]
        ];
        $result = $this->shopifyAPI->rest('PUT', '/admin/products/'.$shopifyID.'/images/'.$imageID.'.json', $data);

        $result = $this->conn->execute('SELECT * FROM products_connector WHERE shopify_product_id = :id', ['id' => $id])->fetchAll('assoc');
        if (!empty($result)) {
            $this->conn->update('products_connector', [
                'shopify_image_id' => $imageID,
            ], ['shopify_product_id' => $id]);
        }

        $done = $this->sendToDatabase($product, $newProduct);
    return true;    
    }

    public function updateProduct($product, $shopifyID) 
    {
        $shopifyProduct = $this->shopifyAPI->rest('GET', '/admin/variants/'.$shopifyID.'.json');
        if ($shopifyProduct['errors'] === true) {
            $this->deleteFromDatabase($product, $shopifyID);
            return false;
        }
        $shopifyProduct = $shopifyProduct['body']->container['variant'];
        $productID = $shopifyProduct['product_id'];

        $description = $this->getRightDescription($product['descriptions']);
        // $state = $this->getState($product['productstate']);

        $state = $this->getState($product['productstate']);
        $brand = $this->getBrand($product['productgroups']);
        $type = $this->getType($product['productgroups']);
        $color = $this->getColor($product);
        $similarity = $this->getSimilarity($product);

        $image = $this->getImage($product['images']);

        $tags = '';
        $tags .= $color;

        $data = [
            "product" => [
                "id" => $productID,
                "title" => $product['name'],
                "body_html" => $description,
                "product_type" => $type,
                "vendor" => $brand,
                "tags" => $tags,
                "variants" => [
                    [
                        "id" => $shopifyID,
                        "title" => $product['name'],
                        "price" => $product['price'],
                        "sku" => $product['code'],
                        "weight" => $product['unitweight'],
                        "weight_unit" => "kg",
                    ],
                ],
            ],
        ];
        $result = $this->shopifyAPI->rest('PUT', '/admin/products/'.$productID.'.json', $data);
        $updated = $result['body']->container['product'];

        $done = $this->sendToDatabase($product, $updated);
        $imageID = $this->addImage($image, $updated);
    return true;
    }

    public function updateProductWithVariants($product, $shopifyID)
    {
        $shopifyProduct = $this->shopifyAPI->rest('GET', '/admin/products/'.$shopifyID.'.json');
        if ($shopifyProduct['errors'] === true) {
            $this->deleteFromDatabase($product, $shopifyID);
            return false;
        }
        $shopifyProduct = $shopifyProduct['body']->container['product'];

        // $state = $this->getState($product['productstate']);

        $description = $this->getDescForPWV($product['product_id']);

        $state = $this->getState($product['productstate']);
        $brand = $this->getBrand($product['productgroups']);
        $type = $this->getType($product['productgroups']);
        $color = $this->getColor($product);
        $similarity = $this->getSimilarity($product);

        // $image = $this->getImage($tehdenVariant['images']);

        $tags = '';
        $tags .= $color;

        $data = [
            "product" => [
                "id" => $shopifyID,
                "title" => $product['name'],
                "body_html" => $description,
                "product_type" => $type, // joko ei ollenkaan tai funktio
                "vendor" => $brand,
                "tags" => $tags,
            ],
        ];

        $result = $this->shopifyAPI->rest('PUT', '/admin/products/'.$shopifyID.'.json', $data);
        $updated = $result['body']->container['product'];

        $done = $this->sendToDatabase($product, $updated);
        $imageID = $this->addImage($image, $updated);
    return true;
    }

    public function updateVariant($product, $shopifyID) 
    {
        $shopifyProduct = $this->shopifyAPI->rest('GET', '/admin/variants/'.$shopifyID.'.json');
        if ($shopifyProduct['errors'] === true) {
            $this->deleteFromDatabase($product, $shopifyID);
            return false;
        }
        $shopifyProduct = $shopifyProduct['body']->container['variant'];

        $parentId = $product['in_selectiongroups'][0]['id'];
        $parentProduct = $this->getProduct($parentId);
        $option = $this->getOptionValue($parentProduct['code'], $product['code']);

        $description = $this->getRightDescription($product['descriptions']);
        
        if ($description != "") {
            $this->updateDescToDatabase($parentId, $description);
        }
        $state = $this->getState($product['productstate']);
        $similarity = $this->getSimilarity($product);

        $image = $this->getImage($product['images']);

        $data = [
            "variant" => [
                "id" => $shopifyID,
                "price" => $product['price'],
                "sku" => $product['code'],
                "option1" => $option,
                "weight" => $product['unitweight'],
                "weight_unit" => "kg",
            ],
        ];
        $result = $this->shopifyAPI->rest('PUT', '/admin/variants/'.$shopifyID.'.json', $data);
        if ($result['errors'] === true) {
            return false;
        }
        $updated = $result['body']->container['variant'];

        $done = $this->sendToDatabase($product, $updated);
        // $imageID = $this->addImage($image, $updated);
    return true;
    }

    public function deleteProduct($product, $shopifyID)
    {
        $shopifyProduct = $this->shopifyAPI->rest('GET', '/admin/variants/'.$shopifyID.'.json');
        if ($shopifyProduct['body'] !== "Not Found") {
            $shopifyProduct = $shopifyProduct['body']->container['variant'];
            $shopifyParentID = $shopifyProduct['product_id'];

            $result = $this->shopifyAPI->rest('DELETE', '/admin/products/'.$shopifyParentID.'.json');
        } else {
            $result['errors'] = false;
        }

        if ($result['errors'] === true) {
            return false;
        } else {
            $result = $this->deleteFromDatabase($product, $shopifyID);
        }
    return true;
    }

    public function deleteProductVariant($product, $shopifyID)
    {
        $shopifyProduct = $this->shopifyAPI->rest('GET', '/admin/variants/'.$shopifyID.'.json');
        if ($shopifyProduct['body'] !== "Not Found") {
            $parentId = $product['in_selectiongroups'][0]['id'];

            $result = $this->conn->execute('SELECT shopify_product_id FROM products_connector WHERE tehden_product_id = :id', ['id' => $parentId])->fetchAll('assoc');
            if (!empty($result) && $result[0]['shopify_product_id'] != 0) {
                $shopifyParentID = $result[0]['shopify_product_id'];
            } else {
                return false;
            }

            $result = $this->shopifyAPI->rest('DELETE', '/admin/products/'.$shopifyParentID.'/variants/'.$shopifyID.'.json');
        } else {
            $result['errors'] = false;
        }

        if ($result['errors'] === true) {
            return false;
        } else {
            $result = $this->deleteFromDatabase($product);
        }
    return true;
    }

    public function deleteProductWithVariants($product, $shopifyID)
    {
        $shopifyProduct = $this->shopifyAPI->rest('GET', '/admin/products/'.$shopifyID.'.json');

        if ($shopifyProduct['body'] !== "Not Found") {
            $result = $this->shopifyAPI->rest('DELETE', '/admin/products/'.$shopifyID.'.json');
        } else {
            $result['errors'] = false;
        }

        if ($result['errors'] === true) {
            return false;
        } else {
            $result = $this->deleteFromDatabase($product);
        }
    return true;
    }

    public function getOptionValue($code, $variantCode)
    {
        if ($code === null) {
            return $variantCode;
        }
        $length = strlen($code);
        $length++;
        if ($variantCode == null) {
            return null;
        }
        $option = substr($variantCode, $length);
        $sizes = Configure::read('Sizechart');
        $change = Configure::read('Secondchart');
        if (in_array($option, $change)) {
            $option = array_search($option,$change,true);
        }
        if (in_array($option, $sizes)) {
            $product = $this->getProductByCode($variantCode);
            $name = $product['name'];
            $option = strval($option);
            if(strpos($name, $option) !== false){
                return $option;
            } else {
                $option = array_search($option,$sizes,true);
            }  
        }
    return $option;
    }

    public function getRightDescription($descriptions)
    {
        $description = '';
        foreach ($descriptions as $desc) {
            if ($desc['language_code'] == "fi") {
                $description .= $desc['customerdescription'];
            }
        }
    return $description; 
    }

    public function getState($state)
    {
        $state = $state;
        return $state;
    }

    public function getBrand($groups)
    {
        $brand = '';
        if ($groups !== null) {
            foreach ($groups as $group) {
                if ($group['code'] !== "9999") {
                    $code = "000";
                    if ($group['code'] !== null) {
                        $code = substr($group['code'], 1);
                    }
                    if ($code !== "000" || $group['name'] == "Tuuma") {
                        $brand = $group['name'];
                    }
                }
            }
        }
    return $brand;
    }

    public function getType($types)
    {
        $mainType = '';
        if ($types !== null) {
            foreach ($types as $type) {
                if ($type['code'] !== "9999") {
                    $code = "nocode";
                    if ($type['code'] != "") {
                        $code = substr($type['code'], 1);
                    }
                    if ($code === "000") {
                        $mainType = $type['name'];
                    } elseif ($code !== "nocode") {
                        $mainTypeCode = substr($type['code'], 0, 1);
                        $groups = $this->getProductGroups();
                        $m = $mainTypeCode . '000';
                        foreach ($groups as $group) {
                            $code = "nocode";
                            if ($group['code'] != "") {
                                $code = $group['code'];
                            }
                            if ($code === $m) {
                                $mainType = $group['name'];
                            }
                        }
                    }
                } else {
                    continue;
                }
            }
        }
    return $mainType;
    }

    public function getColor($product)
    {
        $name = $product['name'];
        $sku = $product['code'];

        if ($sku == null) {
            $logData = print_r($product,true);
            Log::write('debug', 'Product didnt have sku: ' . $product['product_id']);
            // Log::write('debug', $logData);
            return null;
        }

        if (strpos($sku, '-')) {
            $pos = strpos($sku, '-');
            $pos++;
            $c = substr($sku, $pos);

            if (strstr($c, '-', true)) {
                $code = strstr($c, '-', true);
            } else {
                $code = $c;
            }
        } else {
            $code = $sku;
        }

        $colors = Configure::read('Colorchart');
        // $getDataStart = microtime(true);
        // $color = $this->getColor($tehdenProduct);
        $result = $this->conn->execute('SELECT * FROM color_matching WHERE color_code = :code', ['code' => $code])->fetchAll('assoc');
        // $getDataEnd = microtime(true);
        // $getDataTime = $getDataEnd - $getDataStart;
        // print('getData color inner took: ' . $getDataTime . "\n");

        if (empty($result)) {
            $this->conn->insert('color_matching', [
                'color_code' => 'notcolor' . $code,
                'color' => $name,
                'qty' => 1,
            ]);
            $color = '';
        } else {
            $color = $result[0]['color'];   
        }
    return $color;
    }

    public function getSimilarity($product)
    {
        $sku = $product['code'];
        if ($sku == null) {
            Log::write('debug', 'Product didnt have sku: ' . $product['product_id']);
            return null;
        }
        if (strstr($sku, '-', true)) {
            $code = strstr($sku, '-', true);
        } else {
            $code = $sku;
        }
    return $code;
    }

    public function getOptionType($product)
    {
        $type = 'Koko';
    return $type;
    }

    public function addImage($image, $shopifyProduct)
    {
        if ($image === null) {
            return $image;
        }
        $data = [
            "image" => [
                "src" => $image,
                "filename" => 'product_image.jpeg',
            ],
        ];

        $shopifyID = $shopifyProduct['id'];

        $result = $this->shopifyAPI->rest('POST', '/admin/products/'.$shopifyID.'/images.json', $data);
        $image = $result['body']->container['image'];

        $variants = $shopifyProduct['variants'];

        if (!empty($variants)) {
            foreach ($variants as $variant) {
                $id = $variant['id'];

                $data = [
                    "image" => [
                        "id" => $image['id'],
                        "variant_ids" => [
                            $id,
                        ],
                    ]
                ];
                $result = $this->shopifyAPI->rest('PUT', '/admin/products/'.$shopifyID.'/images/'.$image['id'].'.json', $data);

                $result = $this->conn->execute('SELECT * FROM products_connector WHERE shopify_product_id = :id', ['id' => $id])->fetchAll('assoc');
                if (!empty($result)) {
                    $this->conn->update('products_connector', [
                        'shopify_image_id' => $image['id'],
                    ], ['shopify_product_id' => $id]);
                }
            }
        }

        $result = $this->conn->execute('SELECT * FROM products_connector WHERE shopify_product_id = :id', ['id' => $shopifyID])->fetchAll('assoc');

        if (!empty($result)) {
            $this->conn->update('products_connector', [
                'shopify_image_id' => $image['id'],
            ], ['shopify_product_id' => $shopifyID]);
        }
    return $image['id'];    
    }

    public function getImage($images)
    {
        if (!empty($images)) {
            foreach ($images as $img) {
                if ($img['variant'] === "original") {
                    $image = $img['url'];
                }
            }
        } else {
            return null;
        }
    return $image;
    }

    public function updateDescToDatabase($parentId, $description)
    {
        $result = $this->conn->execute('SELECT * FROM products_connector WHERE tehden_product_id = :id', ['id' => $parentId])->fetchAll('assoc');
        if (!empty($result)) {
            $this->conn->update('products_connector', [
                'description' => $description,
            ], ['tehden_product_id' => $parentId]);
        }
    }

    public function getDescForPWV($id)
    {
        $result = $this->conn->execute('SELECT description FROM products_connector WHERE tehden_product_id = :id', ['id' => $id])->fetchAll('assoc');
        if (!empty($result)) {
            $description = $result[0]['description'];
        }
    return $description;
    }

    public function sendToDatabase($tehdenProduct, $shopifyProduct, $parentProduct = null)
    {
        $shopifyID = null;
        $shopifySKU = null;
        $shopifyParentId = null;
        $shopifyInventoryID = null;
        $parentId = null;
        $option = null;
        $description = null;

        if ($shopifyProduct !== null) {
            if ($tehdenProduct['producttype_key'] == "producttype_stock_goods" && empty($tehdenProduct['in_selectiongroups'])) {
                $shopifyProduct = $shopifyProduct['variants'][0];
            }
            $shopifyID = $shopifyProduct['id'];
            if (isset($shopifyProduct['body_html'])) {
                $description = $shopifyProduct['body_html'];
            }
            if (!empty($shopifyProduct['sku'])) {
                $shopifySKU = $shopifyProduct['sku'];
                $shopifyParentId = $shopifyProduct['product_id'];
                $shopifyInventoryID = $shopifyProduct['inventory_item_id'];
            } else {
                $shopifySKU = $tehdenProduct['code'] . '-tehden';
            }
        }

        if (!empty($tehdenProduct['in_selectiongroups'][0]['id'])) {
            if (!isset($parentProduct)) {
                $parentId = $tehdenProduct['in_selectiongroups'][0]['id'];
                // $getProductStart = microtime(true);
                // $product = $this->getProducts($a);
                $parentProduct = $this->getProduct($parentId);
                // $getProductEnd = microtime(true);
                // $getProductTime = $getProductEnd - $getProductStart;
                // Log::write('debug', 'Fetch Tehden product took: ' . $getProductTime);
                // print('Fetch Tehden parent product -> ' . $parentId . ' <-took: ' . $getProductTime . "\n");
            } else {
                $parentId = $parentProduct['product_id'];
                // print('Tehden parent already  -> ' . $parentId . "\n");
            }

            if (!isset($parentProduct['code'])) {
                $parentProduct['code'] = null;
            }

            // $updateDescStart = microtime(true);
            $this->updateDescToDatabase($parentId, $description);
            // $updateDescEnd = microtime(true);
            // $updateDescTime = $updateDescEnd - $updateDescStart;
            // print('Update desc took: ' . $updateDescTime . "\n");
            
            $option = $this->getOptionValue($parentProduct['code'], $tehdenProduct['code']);
        }

        if ($shopifyID !== null) {
            $this->conn->update('products', [
                'moved' => 1,
            ], ['id' => $tehdenProduct['product_id']]);
            debug('New product created');
        } 

        // $getDataStart = microtime(true);
        $state = $this->getState($tehdenProduct['productstate']);
        // $getDataEnd = microtime(true);
        // $getDataTime = $getDataEnd - $getDataStart;
        // print('getData state took: ' . $getDataTime . "\n");
        // $getDataStart = microtime(true);
        $brand = $this->getBrand($tehdenProduct['productgroups']);
        // $getDataEnd = microtime(true);
        // $getDataTime = $getDataEnd - $getDataStart;
        // print('getData getBrand took: ' . $getDataTime . "\n");
        // $getDataStart = microtime(true);
        $type = $this->getType($tehdenProduct['productgroups']);
        // $getDataEnd = microtime(true);
        // $getDataTime = $getDataEnd - $getDataStart;
        // print('getData getType took: ' . $getDataTime . "\n");
        // $getDataStart = microtime(true);
        $color = $this->getColor($tehdenProduct);
        // $getDataEnd = microtime(true);
        // $getDataTime = $getDataEnd - $getDataStart;
        // print('getData color took: ' . $getDataTime . "\n");
        // $getDataStart = microtime(true);
        $similarity = $this->getSimilarity($tehdenProduct);
        // $getDataEnd = microtime(true);
        // $getDataTime = $getDataEnd - $getDataStart;
        // print('getData getSimilarity took: ' . $getDataTime . "\n");
        // $getDataStart = microtime(true);
        $optionType = $this->getOptionType($tehdenProduct);
        // $getDataEnd = microtime(true);
        // $getDataTime = $getDataEnd - $getDataStart;
        // print('getData getOptionType took: ' . $getDataTime . "\n");
        // $getDataStart = microtime(true);
        $tehdenImage = $this->getImage($tehdenProduct['images']);
        // $getDataEnd = microtime(true);
        // $getDataTime = $getDataEnd - $getDataStart;
        // print('getData end took: ' . $getDataTime . "\n");
            // 
        // $selectStart = microtime(true);
        $result = $this->conn->execute('SELECT * FROM products_connector WHERE tehden_product_id = :id', ['id' => $tehdenProduct['product_id']])->fetchAll('assoc');
        // $selectEnd = microtime(true);
        // $selectTime = $selectEnd - $selectStart;
        // print('Select took: ' . $selectTime . "\n");
            // 
        // $updateStart = microtime(true);
        if (empty($result)) {
            $this->conn->insert('products_connector', [
                'tehden_product_id' => $tehdenProduct['product_id'],
                'tehden_product_sku' => $tehdenProduct['code'],
                'tehden_parent_id' => $parentId,
                'shopify_product_id' => $shopifyID,
                'shopify_product_sku' => $shopifySKU,
                'shopify_parent_id' => $shopifyParentId,
                'product_name' => $tehdenProduct['name'],
                'description' => $description,
                'shopify_inventory_id' => $shopifyInventoryID,
                'tehden_image_url' => $tehdenImage,
                'tehden_product_type' => $tehdenProduct['producttype_key'],
                'tehden_available_online' => $tehdenProduct['availableonline'],
                'tehden_product_state' => $state,
                'option_value' => $option,
                'option_type' => $optionType,
                'tehden_brand' => $brand,
                'tehden_type' => $type,
                'tehden_color' => $color,
                'tehden_similarity' => $similarity,
            ]);
        } else {
            $this->conn->update('products_connector', [
                'tehden_product_sku' => $tehdenProduct['code'],
                'tehden_parent_id' => $parentId,
                'shopify_product_id' => $shopifyID,
                'shopify_product_sku' => $shopifySKU,
                'shopify_parent_id' => $shopifyParentId,
                'product_name' => $tehdenProduct['name'],
                'description' => $description,
                'shopify_inventory_id' => $shopifyInventoryID,
                'tehden_image_url' => $tehdenImage,
                'tehden_product_type' => $tehdenProduct['producttype_key'],
                'tehden_available_online' => $tehdenProduct['availableonline'],
                'tehden_product_state' => $state,
                'option_value' => $option,
                'option_type' => $optionType,
                'tehden_brand' => $brand,
                'tehden_type' => $type,
                'tehden_color' => $color,
                'tehden_similarity' => $similarity,
            ], ['tehden_product_id' => $tehdenProduct['product_id']]);       
        }
        // $updateEnd = microtime(true);
        // $updateTime = $updateEnd - $updateStart;
        // print('Update took: ' . $updateTime . "\n");
        return true;
    }

    public function sendVariantData($variant, $tehdenID) 
    {
        $result = $this->conn->execute('SELECT * FROM products_connector WHERE tehden_product_id = :id', ['id' => $tehdenID])->fetchAll('assoc');

        if (empty($result)) {
            $this->conn->insert('products_connector', [
                'tehden_product_id' => $tehdenID,
                'tehden_product_sku' => null,
                'tehden_parent_id' => null,
                'shopify_product_id' => $variant['id'],
                'shopify_product_sku' => $variant['sku'],
                'shopify_parent_id' => $variant['product_id'],
                'product_name' => null,
                'shopify_inventory_id' => $variant['inventory_item_id'],
            ]);
        } else {
            $this->conn->update('products_connector', [
                'shopify_product_id' => $variant['id'],
                'shopify_product_sku' => $variant['sku'],
                'shopify_parent_id' => $variant['product_id'],
                'shopify_inventory_id' => $variant['inventory_item_id'],
            ], ['tehden_product_id' => $tehdenID]);       
        }
    return true;
    }

    public function deleteFromDatabase($tehdenProduct, $shopifyID)
    {
        $result = $this->conn->execute('DELETE FROM products_connector WHERE tehden_product_id = :id AND shopify_product_id = :sid', ['id' => $tehdenProduct['product_id'], 'sid' => $shopifyID]);
    return true;
    }

    public function getShopifyID($tehdenID)
    {
        $result = $this->conn->execute('SELECT shopify_product_id FROM products_connector WHERE tehden_product_id = :id', ['id' => $tehdenID])->fetchAll('assoc');
        if (!empty($result) && $result[0]['shopify_product_id'] != 0) {
            $shopifyID = $result[0]['shopify_product_id'];
        } else {
            return false;
        }
    return $shopifyID;
    }

    public function getUpdatedProducts($sinceParsed)
    {
        if (!empty($sinceParsed)) {
            $this->tehdenUrlParams['timestamp_since'] = $sinceParsed;
        }

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'product/getAllProducts', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $result = $result->getJson();
        $ids = [];

        foreach ($result as $p) {
            $ids[] = $p['product_id'];
        }

    return $ids;
    }

    public function getProducts($ids)
    {
        $this->tehdenUrlParams['product_ids'] = $ids;

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'product/getProducts', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $products = $result->getJson();

        if ($products === null || isset($products['code']) && $products['code'] === "fail") {
            return false;
        }
        $products = isset($products['result']) ? $products['result'] : null;

        return $products;
    }

    public function getProduct($id)
    {
        $this->tehdenUrlParams['product_id'] = $id;

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'product/getById', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $product = $result->getJson();

    return $product;
    }

    public function getProductsInGroup($groupId)
    {
        $this->tehdenUrlParams['group_id'] = $groupId;

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'productgroup/getProductsInGroup', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $group = $result->getJson();

    return $group;
    }

    public function getProductGroups()
    {
        if (!$this->productGroups) {

            $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'productgroup/getProductGroups', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

            $this->productGroups = $result->getJson();
        }

        return $this->productGroups;
    }


    public function getImages($id)
    {
        $this->tehdenUrlParams['product_id'] = $id;

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'product/getProductImages', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $images = $result->getJson();
        dd($images);

        if (empty($products)) {
            return false;
        }
        $products = $products['result'];

    return $products;
    }

    public function getProductByCode($code)
    {
        $this->tehdenUrlParams['code'] = $code;
        $this->tehdenUrlParams['codetype'] = 'code';


        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'product/getProductIdByCode', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $productID = $result->getJson();
        $product = $this->getProduct($productID);

    return $product;
    }

    public function getNewToken()
    {
        $this->tehdenToken = $this->tehdenAPI->get(
            'https://suomenbrodeeraus.tehden.com/oauth/oaclient/token', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );
        $this->tehdenToken = $this->tehdenToken->getJson();
        $this->tehdenToken = $this->tehdenToken['access_token'];
        $this->tehdenToken = base64_encode($this->tehdenToken);

        $this->tehdenDefConfig['headers'] = [
            'Accept' => 'application/vnd.tehden.api-v1+json',
            'Accept-language' => 'fi',
            'Authorization' => 'Bearer '. $this->tehdenToken,
        ];
    return true;
    }
}