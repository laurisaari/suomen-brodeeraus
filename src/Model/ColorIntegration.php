<?php
declare(strict_types=1);

namespace App\Model;

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Log\Log;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;
use Cake\Http\Client;
use Cake\Datasource\ConnectionManager;
use DateTime;

/**
 * Integration logic for moving product stock data from Maestro API to customized Prestashop API
 */
class ColorIntegration
{
    private $shopifyAPI = null;
    private $tehdenAPI = null;
    private $tehdenDefConfig = [];
    private $tehdenUrlParams = [];
    private $tehdenToken = null;
    private $tehdenUrl = null;

    /**
     * Setup integration pipeline
     *
     * @param \Psr\Http\Client\ClientInterface|\Cake\Http\Client|null $shopifyAPI HTTP client for Shopify REST API
     * @param \Psr\Http\Client\ClientInterface|\Cake\Http\Client|null $tehdenAPI HTTP client for Tehden REST API
     */
    public function __construct(
        $shopifyAPI, 
        $tehdenAPI
    ) {
        //Shopify client 
        $this->shopifyAPI = $shopifyAPI;

        //Tehden client
        $this->tehdenAPI = $tehdenAPI;

        $this->tehdenUrl = Configure::read('Tehden.api.url');

        $this->tehdenUrlParams = [
            'grant_type' => 'password',
            'username' => Configure::read('Tehden.api.username'),
            'password' => Configure::read('Tehden.api.password'),
            'redirect_url' => Configure::read('Tehden.api.redirect_url'),
            'client_id' => Configure::read('Tehden.api.client_id'),
            'client_secret' => Configure::read('Tehden.api.client_secret'),
        ];

        $this->tehdenToken = $this->tehdenAPI->get(
                'https://suomenbrodeeraus.tehden.com/oauth/oaclient/token', // URL
                    $this->tehdenUrlParams, // URL "data" = query parameters
                    $this->tehdenDefConfig,
                );
        $this->tehdenToken = $this->tehdenToken->getJson();
        $this->tehdenToken = $this->tehdenToken['access_token'];
        $this->tehdenToken = base64_encode($this->tehdenToken);

        $this->tehdenDefConfig['headers'] = [
            'Accept' => 'application/vnd.tehden.api-v1+json',
            'Accept-language' => 'fi',
            'Authorization' => 'Bearer '. $this->tehdenToken,
        ];

        //Database connection
        // $this->conn = ConnectionManager::get('default');
    }

    public function bringProducts()
    {
        // $products = $this->conn->execute('SELECT id FROM products WHERE moved = 0')->fetchAll('assoc');

        $start = strtotime('now');
        $stop = strtotime('+2 second');

        // dd($start, $stop);

        while ($start < $stop) {
            debug(strtotime('now'));
            $start = strtotime('now');
        }

        // $count = count($products);

        // foreach ($products as $i => $product) {

        //     debug($i . '/' . $count);

        //     // $result = $this->conn->execute('SELECT shopify_product_id FROM products_connector WHERE tehden_product_id = :id', ['id' => $product['id']])->fetchAll('assoc');

        //     // if ($result[0]['shopify_product_id'] !== null) {
        //     //     $this->conn->update('products', [
        //     //         'moved' => 1,
        //     //     ], ['id' => $product['id']]);
        //     // } else {
        //     //     continue;
        //     // }
        // }

        // $result = $this->tehdenAPI->get(
        //         $this->tehdenUrl.'product/getAllProducts', // URL
        //         $this->tehdenUrlParams, // URL "data" = query parameters
        //         $this->tehdenDefConfig,
        //     );

        // $result = $result->getJson();


        // foreach ($result as $i => $product) {
        //     debug($i);
        //     $this->conn->insert('products', [
        //         'id' => $product['product_id'],
        //         'code' => $product['code'],
        //         'moved' => false,
        //     ]);
        // }
    }

    /**
     * Main method of the stock update pipeline
     *
     * @param string $since String of update range start, will be converted to DateTime object
     *
     * @throws \Exception Throws Exception on error
     *
     * @return bool True on successful update
     */
    public function updateColors()
    {
        if (!$this->shopifyAPI) {
            throw new \Exception('Shopify client missing', 1);
        }

        if (!$this->tehdenAPI) {
            throw new \Exception('Tehden cliet missing', 1);
        }

        if (!$this->conn) {
            throw new \Exception('Database connection missing', 1);
        }

        $CVS = Configure::read('CVS');
        $bigData = [];

        foreach ($CVS as $f) {
            $file = fopen("/var/www/html/tehden-shopify-integraatio/".$f.".csv", "r");

            $other = [
                "Printer",
                "HarvestFrost",
                "Harvest",
                "Grizzly",
                "Dad",
                "Cottover",
                "Printer",
                "SpecialAssortment",
                "Sagaform",
                "Projob",
                "Cutterandbuck",
                "Craft",
                "Clique",
            ];

            $fristads = [
                "Fristads3",
                "Fristads2",
            ];

            if (in_array($f, $other)) {
                $i = 1;
                while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {

                    if ($i < 2) {
                        $i++;
                        continue;
                    } else {
                        if (!isset($data[23])) {
                            continue;
                        }
                        $smallData = [
                            $data[2],
                            $data[23]
                        ];
                        $bigData[] = $smallData;
                    }
                }
            }

            if (in_array($f, $fristads)) {
                $i = 0;
                while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {

                    if ($i < 6) {
                        $i++;
                        continue;
                    } else {
                        $pos = strpos($data[0], '-');
                        if ($pos !== false) {
                            $pos++;
                            $code = substr($data[0], $pos);
                        } else {
                            $code = $data[0];
                        }
                        $smallData = [
                            $code,
                            $data[3]
                        ];
                        $bigData[] = $smallData;
                    }
                }
            }

            if ($f === "Fristads") {
                while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {

                    if ($data[0] === "Koodi") {
                        continue;
                    } else {
                        $smallData = [
                            $data[0],
                            $data[1]
                        ];

                        $bigData[] = $smallData;
                    }
                }
            }
        }
        $this->toDatabase($bigData);
    }

    public function toDatabase($bigData)
    {
        foreach ($bigData as $d) {
            $code = $d[0];
            $color = $d[1];

            if ($code == "" && $color == "") {
                continue;
            }

            $result = $this->conn->execute('SELECT * FROM color_matching WHERE color_code = :code', ['code' => $code])->fetchAll('assoc');

            if (empty($result)) {
                $this->conn->insert('color_matching', [
                    'color_code' => $code,
                    'color' => $color,
                    'qty' => 1,
                ]);
            } else {
                $qty = $result[0]['qty'] + 1;
                if ($result[0]['color'] == "" || $result[0]['color'] == "Poistuva 2021") {
                    $this->conn->update('color_matching', [
                        'color' => $color,
                        'qty' => $qty,
                    ], ['color_code' => $code]);       
                } else {
                    $this->conn->update('color_matching', [
                        'qty' => $qty,
                    ], ['color_code' => $code]);  
                }
            }
        }
    }
}    