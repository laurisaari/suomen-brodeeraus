<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Console\ConsoleOptionParser;
use Cake\Log\Log;
use Psy\Shell as PsyShell;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;
use App\Model\ProductIntegration;
use App\Model\StockIntegration;
use App\Model\OrderIntegration;
use Cake\Http\Client;
use DateTime;
use Cake\Datasource\ConnectionManager;
use transit_realtime\FeedMessage;

/**
 * Simple console wrapper around Psy\Shell.
 */
class TestCommand extends Command
{
    /**
     * Start the Command and interactive console.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return int|null|void The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $since = $args->getOption('since');

        // Create options for the API
        $options = new Options();
        $options->setVersion('2021-04');
        $options->setApiKey(Configure::read('Shopify.api.key'));
        $options->setApiPassword(Configure::read('Shopify.api.password'));

        // Create the client and session
        $shopifyAPI = new BasicShopifyAPI($options);

        $shopifyAPI->setSession(new Session(Configure::read('Shopify.api.url'), Configure::read('Shopify.api.password')));

        $tehdenAPI = new Client;

        $this->tehdenDefConfig = [];

        $this->tehdenUrlParams = [];

        $this->tehdenAPI = $tehdenAPI;

        $this->tehdenUrl = Configure::read('Tehden.api.url');

        $this->tehdenUrlParams = [
            'grant_type' => 'password',
            'username' => Configure::read('Tehden.api.username'),
            'password' => Configure::read('Tehden.api.password'),
            'redirect_url' => Configure::read('Tehden.api.redirect_url'),
            'client_id' => Configure::read('Tehden.api.client_id'),
            'client_secret' => Configure::read('Tehden.api.client_secret'),
        ];

        $this->tehdenToken = $this->tehdenAPI->get(
                'https://suomenbrodeeraus.tehden.com/oauth/oaclient/token', // URL
                    $this->tehdenUrlParams, // URL "data" = query parameters
                    $this->tehdenDefConfig,
                );
        $this->tehdenToken = $this->tehdenToken->getJson();
        $this->tehdenToken = $this->tehdenToken['access_token'];
        $this->tehdenToken = base64_encode($this->tehdenToken);

        $this->tehdenDefConfig['headers'] = [
            'Accept' => 'application/vnd.tehden.api-v1+json',
            'Accept-language' => 'fi',
            'Authorization' => 'Bearer '. $this->tehdenToken,
        ];

        // Create options for the API
        $options = new Options();
        $options->setVersion('2021-04');
        $options->setApiKey(Configure::read('Shopify.api.key'));
        $options->setApiPassword(Configure::read('Shopify.api.password'));

        // Create the client and session
        $shopifyAPI = new BasicShopifyAPI($options);

        $shopifyAPI->setSession(new Session(Configure::read('Shopify.api.url'), Configure::read('Shopify.api.password')));

        //Database connection
        $this->conn = ConnectionManager::get('default');

        if (!empty($since)) {
            // Try parsing since range start, let it crash if incorrent/unparseable
            $sinceParsed = (new \DateTime($since))->format(
                \DateTimeInterface::ISO8601
            );
            // TODO Sanity check that parsed date is not in future or too far in past
            $sinceParsed = strtotime($since);
        }

        if (!empty($sinceParsed)) {
            $this->tehdenUrlParams['timestamp_since'] = $sinceParsed;
        }

        // Moving check
        $ids = $this->conn->execute('SELECT id FROM products WHERE moved = 0')->fetchAll('assoc');

        $count = count($ids);
        dd($count);
        $p = 1;

        foreach ($ids as $index => $id) {

            if ($index < 117859) {
                Log::write('debug', $p . '/' . $count);
                $p++;
                continue;
            }

            $shopifyID = $this->getShopifyID($id['id']);

            // if (isset($product['in_selectiongroups'][0]['id'])) {
            //     $parentId = $product['in_selectiongroups'][0]['id'];
            //     $parentProduct = $this->getProduct($parentId);
            //     $option = $this->getOptionValue($parentProduct['code'], $product['code']);
            //     dd($option);
            // } elseif ($shopifyID === false) {
            //     debug('didnt ' . $p . '/' . $count);
            //     $p++;
            //     continue;
            // }

            $variant = $shopifyAPI->rest('GET', '/admin/variants/'.$shopifyID.'.json');

            if ($variant["body"] == "Not Found") {
                $product = $shopifyAPI->rest('GET', '/admin/products/'.$shopifyID.'.json');
                if ($product["body"] == "Not Found") {
                    Log::write('debug', 'didnt find product: '.$id['id']. ' ' . $p . '/' . $count);
                    $this->conn->update('products', [
                        'moved' => 0,
                    ], ['id' => $id['id']]);
                    $p++;
                    continue;
                }
            }

            // if ($p !== 0 && fmod($p, 10000) === 0.0) {
            //     $this->getNewToken();
            // }

            // if ($shopifyID !== false) {
            //     $this->conn->update('products', [
            //         'moved' => 1,
            //     ], ['id' => $id['id']]);
            //     debug('moved ' . $p . '/' . $count);
            //     $p++;
            //     $now = strtotime('now');
            //     continue;
            // } 
            // else {
            //     $result = $this->conn->execute('SELECT shopify_product_id FROM products_connector WHERE tehden_product_id = :id', ['id' => $parentId])->fetchAll('assoc');
            //     if ($result[0]['shopify_product_id'] !== null) {
            //         $shopifyID = $result[0]['shopify_product_id'];

            //         $options = $this->conn->execute('SELECT option_value  FROM products_connector WHERE tehden_parent_id = :id', ['id' => $parentId])->fetchAll('assoc');

            //         foreach ($options as $key => $o) {
            //             if ($o['option_value'] == $option) {
            //                 $this->conn->update('products', [
            //                     'moved' => 1,
            //                 ], ['id' => $product['product_id']]);
            //                 return false;
            //             }
            //         }
            //     }
            // }
        Log::write('debug', $p . '/' . $count);
        $p++;
        continue;
        }

        dd('done');


        // $this->tehdenUrlParams['product_id'] = "83afb26c-9161-11eb-bad7-fa163ec26693";

        // $result = $this->tehdenAPI->get(
        //         $this->tehdenUrl.'product/getById', // URL
        //         $this->tehdenUrlParams, // URL "data" = query parameters
        //         $this->tehdenDefConfig,
        //     );

        // $product = $result->getJson();

        // dd($product);

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'product/getAllProducts', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $result = $result->getJson();

        dd($result);

        // $shopifyProduct = $shopifyAPI->rest('GET', '/admin/products.json');
        // $pipeline = new OrderIntegration(
        //     $shopifyAPI,
        //     $tehdenAPI
        // );

        $pipeline->updateOrders($since);
    }

    /**
     * Display help for this console.
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to update
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        $parser
            ->addOption(
                'since',
                ['help' => 'Update range start date, wrap in quotes if countains spaces']
            );

        return $parser;
    }

    public function getShopifyID($tehdenID)
    {
        $result = $this->conn->execute('SELECT shopify_product_id FROM products_connector WHERE tehden_product_id = :id', ['id' => $tehdenID])->fetchAll('assoc');
        if (!empty($result) && $result[0]['shopify_product_id'] != 0) {
            $shopifyID = $result[0]['shopify_product_id'];
        } else {
            return false;
        }
    return $shopifyID;
    }

    public function getNewToken()
    {
        $this->tehdenToken = $this->tehdenAPI->get(
            'https://suomenbrodeeraus.tehden.com/oauth/oaclient/token', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );
        $this->tehdenToken = $this->tehdenToken->getJson();
        $this->tehdenToken = $this->tehdenToken['access_token'];
        $this->tehdenToken = base64_encode($this->tehdenToken);

        $this->tehdenDefConfig['headers'] = [
            'Accept' => 'application/vnd.tehden.api-v1+json',
            'Accept-language' => 'fi',
            'Authorization' => 'Bearer '. $this->tehdenToken,
        ];
    return true;
    }

    public function getProducts($ids)
    {
        $this->tehdenUrlParams['product_ids'] = $ids;

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'product/getProducts', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $products = $result->getJson();

        if ($products === null || $products['code'] === "fail") {
            return false;
        }
        $products = $products['result'];

    return $products;
    }

    public function getProduct($id)
    {
        $this->tehdenUrlParams['product_id'] = $id;

        $result = $this->tehdenAPI->get(
                $this->tehdenUrl.'product/getById', // URL
                $this->tehdenUrlParams, // URL "data" = query parameters
                $this->tehdenDefConfig,
            );

        $product = $result->getJson();

    return $product;
    }

    public function getOptionValue($code, $variantCode)
    {
        if ($code === null) {
            return $variantCode;
        }
        $length = strlen($code);
        $length++;
        if ($variantCode == null) {
            return null;
        }
        $option = substr($variantCode, $length);
        $sizes = Configure::read('Sizechart');
        $change = Configure::read('Secondchart');
        if (in_array($option, $change)) {
            $option = array_search($option,$change,true);
        }
        if (in_array($option, $sizes)) {
            $product = $this->getProductByCode($variantCode);
            $name = $product['name'];
            $option = strval($option);
            if(strpos($name, $option) !== false){
                return $option;
            } else {
                $option = array_search($option,$sizes,true);
            }  
        }
    return $option;
    }
}
