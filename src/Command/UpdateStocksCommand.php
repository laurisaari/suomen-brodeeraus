<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Console\ConsoleOptionParser;
use Cake\Log\Log;
use Psy\Shell as PsyShell;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;
use App\Model\ProductIntegration;
use App\Model\StockIntegration;
use Cake\Http\Client;
use DateTime;

/**
 * Simple console wrapper around Psy\Shell.
 */
class UpdateStocksCommand extends Command
{
    /**
     * Start the Command and interactive console.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return int|null|void The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $since = $args->getOption('since');

        // Create options for the API
        $options = new Options();
        $options->setVersion('2021-04');
        $options->setApiKey(Configure::read('Shopify.api.key'));
        $options->setApiPassword(Configure::read('Shopify.api.password'));

        // Create the client and session
        $shopifyAPI = new BasicShopifyAPI($options);

        $shopifyAPI->setSession(new Session(Configure::read('Shopify.api.url'), Configure::read('Shopify.api.password')));

        $tehdenAPI = new Client;

        $pipeline = new StockIntegration(
            $shopifyAPI,
            $tehdenAPI
        );

        $pipeline->updateStocks($since);
    }

    /**
     * Display help for this console.
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to update
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        $parser
            ->addOption(
                'since',
                ['help' => 'Update range start date, wrap in quotes if countains spaces']
            );

        return $parser;
    }
}
